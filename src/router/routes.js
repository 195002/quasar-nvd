
const routes = [
 {
   path: '/',
   component: () => import('src/layouts/MainLayout.vue'),
    children:
   [
      { path: '/', component: () => import('pages/IndexPage.vue') },
      { path: '/laptops', component: () => import('pages/LaptopPage.vue') },
      { path: '/tv', component: () => import('pages/TvPage.vue') },
      { path: '/form', component: () => import('pages/FormPage.vue') }
    ]
 },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
